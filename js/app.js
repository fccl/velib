(function () {

    // l'URL de l'API v2 JCDecaux : à préciser obligatoirement pour récupérer les informations sur les stations
    const API_URL = '';

    // initialise la map
    function initMap(stations) {

        let map = L.map('map');
        L.tileLayer('//{s}.tile-cyclosm.openstreetmap.fr/cyclosm/{z}/{x}/{y}.png', {
            attribution: ' <a href="https://www.cyclosm.org">CyclOSM</a> | © <a href="https://www.openstreetmap.org">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0">CC-BY-SA</a>, © <a href="https://www.openstreetmap.fr">OSM France</a>',
            minZoom: 1,
            maxZoom: 20
        }).addTo(map);

        let icon = L.icon({
            iconUrl: "images/marker-bike.png",
            iconSize: [32, 32],
            iconAnchor: [16, 32],
            popupAnchor: [0, -32]
        });

        let cluster = L.markerClusterGroup();
        let markers = [];
        let marker;
        stations.forEach(function(station) {
            marker = L.marker([station.position.latitude, station.position.longitude], {icon: icon});
            marker.bindPopup(
                '<h3>Station n° ' + station.number +'</h3>' +
                '<h2 class="text-primary">' + station.address + '</h2>' +
                '<p>Nombre de vélos : <strong>' + station.bike_stands + '</strong><br>' +
                'Vélos disponibles : <strong class="text-success">' + station.available_bikes + '</strong><br>' +
                'Points d\'attache disponibles : <strong class="text-success">' + station.available_bike_stands + '</strong></p>'
            );
            cluster.addLayer(marker);
            markers.push(marker);
        });

        // géolocalisation (uniquement si https)
        if (location.protocol === 'https:') {
            L.control.locate({
                strings: {
                    title: "Me positionner sur la carte",
                    metersUnit: "mètre(s)",
                    popup: "Vous êtes à {distance} {unit} de ce point"
                },
                follow: true,
                locateOptions: {
                    maxZoom: 14
                }
            }).addTo(map);
        }

        let group = new L.featureGroup(markers);
        map.fitBounds(group.getBounds().pad(0));
        map.addLayer(cluster);
    }

    // requête pour récupérer les stations
    function makeRequest() {
        if (!API_URL) {
            alert("Il faut obligatoirement renseigner l'URL de l'API  JCDecaux : constante API_URL dans le fichier app.js :-)")
        }
        let httpRequest = new XMLHttpRequest();
        httpRequest.onreadystatechange = function () {
            if (httpRequest.readyState === XMLHttpRequest.DONE) {
                if (httpRequest.status === 200) {
                    initMap(JSON.parse(httpRequest.responseText));
                } else {
                    alert('Il y a eu un problème lors de la récupération des stations.');
                }
            }
        };
        httpRequest.open('GET', API_URL);
        httpRequest.send();
    }

    makeRequest();
})();
