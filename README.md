# Application velib Nancy

**Informations**

- Ce projet permet d'afficher sur une carte [OpenStreetMap](openstreetmap.org) des informations sur les stations des vélos en libre-service (vélostan) sur Nancy
- Il tilise l'API open data de [JCDecaux](https://developer.jcdecaux.com)

**Prérequis**

- Faire une demande de token sur https://developer.jcdecaux.com (nécessite un compte gratuit)
- Plus d'infos sur l'utilisation du token : https://developer.jcdecaux.com/#/opendata/vls?page=getstarted

**Installation**

- Cloner le dépôt
- Dans la constante API_URL du fichier app.js, positionner l'URL (version 2) de l'API JCDecaux avec votre token : https://api.jcdecaux.com/vls/v2/stations?contract=nancy&apiKey=ICI_TOKEN
- C'est tout (normalement) :)

**Evoltions**

- D'autres fonctionnalités sont prévues comme une liste déroulante avec les différentes stations...